package com.gl.movieRatingApp;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class ListOfMovies {

	private static Connection connection;
	private static Statement createStatement;
	private static String updateQuery = "";
	
	private static Scanner scanner;

	public static void viewAllMovies() throws Exception
	{
		scanner = DatabaseConnection.getScanner();
		
		
		
		String selectQuery = "select * from movie";
		

		connection = DatabaseConnection.getConnection();
		createStatement = connection.createStatement();
		ResultSet rs = createStatement.executeQuery(selectQuery);

		while(rs.next())
		{
			System.out.println("Movie id: "+rs.getInt(1) +"\n Movie Name: "+rs.getString(2)+"\n Movie Ratings: "+rs.getInt(3)+"\n");
		}
		

	}
	
	public static void rateMovie(String username) throws Exception
	{
		System.out.println("\nEnter the movie name which you want to rate");
		String movieName = scanner.nextLine();
		System.out.println("please enter rating for the selected movie from 1 to 10");
		int rating = scanner.nextInt();
		if(rating>0 && rating<=10)
		{
			
			
			updateQuery = "insert into movie.movie(moviename,movierating,username) values('"+movieName+"',"+rating+",'"+username+"');";
			int updated = createStatement.executeUpdate(updateQuery);
			if(updated > 0)
			{
				System.out.println("Movie ratings updated successfully");
			}else
			{
				System.out.println("Failed to update movie ratings");
			}
		}
		else
		{
			System.out.println("Please enter valid rating from 1 to 10");
		}
	}
	public static void getAllMoviesOfUser(String userName) throws Exception
	{
		
		String selectQuery = "select * from movie.movie where username='"+userName+"'";
		ResultSet resultSet = createStatement.executeQuery(selectQuery);
		System.out.println("Here are the list of you favourite Movies");
		while(resultSet.next())
		{
			String movieName = resultSet.getString(2);
			int movieRating = resultSet.getInt(3);
			String MovieUserName = resultSet.getString(4);
			
			
			System.out.println("Movie Name: "+movieName+"\n Movie Ratings: "+movieRating+"\n UserName: "+MovieUserName+"\n");
		}
	}
	
	
}
















