package com.gl.movieRatingApp;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.InputMismatchException;
import java.util.Scanner;

public class App 
{
	static Scanner scan = null;
	static String userName;
	static String password;
	static Connection connection = null;
	static Statement statement = null ;
	static Boolean flag = false;

	private static String userLoggedIn = null;


	public static void main( String[] args ) throws Exception
	{
		connection = DatabaseConnection.getConnection();

		scan = DatabaseConnection.getScanner();

		System.out.println("1. Press 1 if you are a new user");
		System.out.println("2. Press 2 if you are an existing user");
		System.out.println("3. Press 3 if you want to view list of movies");


		int i = 0;
		try {
			i = scan.nextInt();
		} 
		
		catch (InputMismatchException e) {
			System.out.println("Please enter valid number");
		}
		
		catch (Exception e) {
			// TODO: handle exception
			System.out.println("");
		}


		switch (i) {
		case 1:App.signup();

		case 2:App.login();


		case 3:ListOfMovies.viewAllMovies();


		case 4:if(flag)
		{
			int n = 0;
			try
			{
				System.out.println("Press 1 to rate a movie");
				System.out.println("Press 2 to add a movie");
				System.out.println("Press 3 to view my favourite movies");
				n = scan.nextInt();
			}
			catch (InputMismatchException e) {
				System.out.println("Please enter valid number");
			}
			catch (Exception e) {
				System.out.println("some exception");
				e.printStackTrace();
			}
			switch(n)
			{
			case 1: ListOfMovies.rateMovie(userLoggedIn);
			break;
			case 2: AddMovie.addMoive();
			break;
			case 3: ListOfMovies.getAllMoviesOfUser(userLoggedIn);


			}
		}
		else
		{
			System.out.println("please login");
		}
		break;
		default: System.out.println("Invalid Entry");
		}
	}


	public static void login() {
		scan = new Scanner(System.in);
		System.out.println("Please Login!");
		System.out.println("Enter Username");
		userLoggedIn = scan.next();
		System.out.println("Enter Password");
		String p = scan.next();

		try 
		{
			statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("select * from user where username='"+userLoggedIn+"' and password='"+p+"'");

			if(rs.next())
			{
				System.out.println("***************************Login Successful***********************************");
				System.out.println("*************************** Welcome "+ userLoggedIn+"*************************");
				flag = true;



			}
			else
			{
				System.out.println("Login Failed");
				flag = false;
			}

		}
		catch (SQLException e) 
		{
			System.out.println("Exception in login");
			e.printStackTrace();
		}
	}


	public static void signup() {
		scan = new Scanner(System.in);
		System.out.println("Please Register!!");
		System.out.println("Enter Username : ");
		userName = scan.next();
		System.out.println("Enter Password : ");
		password = scan.next();
		try {
			statement = connection.createStatement();
			int i = statement.executeUpdate("insert into user values('"+userName+"','"+password+"')");
			if(i==1)
			{
				System.out.println("User Registered Succcessfully");
			}
			else
			{
				System.out.println("Failed to Register \n Please try again later");
			}
		}
		catch(SQLIntegrityConstraintViolationException si)
		{
			System.out.println("User already exists!! Please register with different username");
			signup();
		}
		catch (SQLException e) 
		{
			System.out.println("Failed to Register \n Please try again later \n"+ e);
		}
	}


}
