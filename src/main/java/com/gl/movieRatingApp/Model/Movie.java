package com.gl.movieRatingApp.Model;

public class Movie {

	private String movieName;
	private int yearOfRealease;
	private int movieStarring;
	private int movieRating;
	
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public int getYearOfRealease() {
		return yearOfRealease;
	}
	public void setYearOfRealease(int yearOfRealease) {
		this.yearOfRealease = yearOfRealease;
	}
	public int getMovieStarring() {
		return movieStarring;
	}
	public void setMovieStarring(int movieStarring) {
		this.movieStarring = movieStarring;
	}
	public int getMovieRating() {
		return movieRating;
	}
	public void setMovieRating(int movieRating) {
		this.movieRating = movieRating;
	}
	@Override
	public String toString() {
		return "Movie [movieName=" + movieName + ", yearOfRealease=" + yearOfRealease + ", movieStarring="
				+ movieStarring + ", movieRating=" + movieRating + "]";
	}
	public Movie(String movieName, int yearOfRealease, int movieStarring, int movieRating) {
		super();
		this.movieName = movieName;
		this.yearOfRealease = yearOfRealease;
		this.movieStarring = movieStarring;
		this.movieRating = movieRating;
	}
	public Movie() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}
