package com.gl.movieRatingApp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Scanner;

public class DatabaseConnection {

	static Connection connection = null;
	static Scanner scan = null;
	
	//Creating Singleton Object
	public static Connection getConnection() throws Exception
	{
		
		Class.forName("com.mysql.cj.jdbc.Driver");
		String url = "jdbc:mysql://localhost:3306/movie";
		String username = "root";
		String password = "password";
		
		if(connection == null)
		{
			connection = DriverManager.getConnection(url,username,password);
			return connection;
		}
		else
		{
			return connection;
		}
		
		
	}
	
	//Creating Singleton Object
	public static  Scanner getScanner()
	{
		if(scan == null)
		{
			scan = new Scanner(System.in);
			return scan;
		}
		else
		{
			return scan ;
		}
	}
}
